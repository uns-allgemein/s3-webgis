// imports
import images from './*.jpg';
import MapboxVector from 'ol/layer/MapboxVector';
import infoIcon from "/assets/info.png";
import 'ol/ol.css';
import Map from 'ol/Map';
import {Vector as VectorSource} from 'ol/source';
import View from 'ol/View';
import GeoJSON from 'ol/format/GeoJSON';
import {Vector as VectorLayer} from 'ol/layer';
import {Icon, Style} from 'ol/style';
import Select from 'ol/interaction/Select';
import {click} from 'ol/events/condition';

//Flag
var darkMode = false;
const accessTokenV = 'pk.eyJ1IjoidW5kZWZpbmVkc29sdXRpb25zIiwiYSI6ImNrazE3cTB4NzBvbXEydm81cW5kN2U0eXIifQ.dxb1TdUSvbr9sgPIeclEGg';
const elementeGeoJSON = require('./elemente.json');

// Styles

var styleIcon = new Style({
  image: new Icon({
    anchor: [64,64],
    anchorXUnits: 'pixels',
    anchorYUnits: 'pixels',
    src: infoIcon,
    scale: 0.17,
  })
});

var styleIconSelect = new Style({
  image: new Icon({
    anchor: [64,64],
    anchorXUnits: 'pixels',
    anchorYUnits: 'pixels',
    src: infoIcon,
    scale: 0.27,
  })
});

// VectorLayer
var vectorSource = new VectorSource({
  features: new GeoJSON().readFeatures(elementeGeoJSON,{
    featureProjection: 'EPSG:3857'
  }),
});
var vectorLayer = new VectorLayer({
  source: vectorSource,
  style: styleIcon,
});

// MapboxLayer
var mapBoxDark = new MapboxVector({
  styleUrl: 'mapbox://styles/mapbox/dark-v10',
  accessToken: accessTokenV
});
var mapBox = new MapboxVector({
  styleUrl: 'mapbox://styles/mapbox/bright-v9',
  accessToken: accessTokenV
});

//Map
var map = new Map({
  layers: [
    mapBox,
    vectorLayer
  ],
  target: 'map',
  view: new View({
    center: [0, 0],
    zoom: 2,
  }),
});
// Zoom to Elements
map.getView().fit(vectorSource.getExtent(),{padding:[100,100,100,100],maxZoom:20});



// Select Element on Map
var select = new Select({
  condition: click,
  layers: [vectorLayer],
  hitTolerance: 5,
  style: styleIconSelect
});
map.addInteraction(select);

select.on('select',function(e){
  var index = 0
  if(e.selected.length > 0){
    var sel = e.selected[0];
    // get Index of Element
     index = elementeGeoJSON.features.map(function(e) { return e.properties.title; }).indexOf(sel.get('title'));
     
  }
  selectElement(index);
});
// Select Element on list
function click2liElement(event){
  var index = elementeGeoJSON.features.map(function(e) { return e.properties.title; }).indexOf(event.target.title);
  selectElement(index);
  
  var features = vectorSource.getFeatures();
  select.getFeatures().clear();
  for (var i = 0, ii = features.length; i < ii; i++) {
    if (features[i].get('title') === event.target.title){
      select.getFeatures().push(features[i])
    }
  }
}
// selectFunction for list and map
function selectElement(index){
  document.querySelectorAll('img')[0].src = images[elementeGeoJSON.features[index].properties.image.split('.')[0]];
  document.querySelectorAll('h2')[0].innerHTML = elementeGeoJSON.features[index].properties.title
  document.querySelectorAll('p')[0].innerHTML = elementeGeoJSON.features[index].properties.content

  // Remove from all the focus and add to the selected
  var elements = document.getElementsByClassName('focus');
  while(elements.length > 0){
    elements[0].classList.remove('focus');
  }
  document.querySelectorAll('li[title="'+elementeGeoJSON.features[index].properties.title+'"]')[0].classList.add("focus");
}

// Add all elements to the Navigation
elementeGeoJSON.features.forEach(item => {
  var ul = document.querySelectorAll('ul')[0];
  var li = document.createElement("li");
  var title = item.properties.title
  li.appendChild(document.createTextNode(title));
  li.title = title
  ul.appendChild(li);
  li.addEventListener('click', click2liElement);
})

// toggleDarkmode
document.getElementById('darkmode').addEventListener('click',switchDarkmode);
function switchDarkmode(){
  map.getLayers().removeAt(0);
  var r = document.querySelector(':root');
  var rs = getComputedStyle(r);
  darkMode = !darkMode;
  if(darkMode){
    map.getLayers().insertAt(0,mapBoxDark);
    document.getElementById('map').style.backgroundImage = "url(./assets/map_dark.jpg)";
    r.style.setProperty('--main', rs.getPropertyValue('--white'));
    r.style.setProperty('--background', rs.getPropertyValue('--almost-black'));
    r.style.setProperty('--invBackground', rs.getPropertyValue('--white'));
    r.style.setProperty('--accentBackground', rs.getPropertyValue('--black'));
  } else{
    map.getLayers().insertAt(0,mapBox);
    document.getElementById('map').style.backgroundImage = "url(./assets/map_topo.jpg)";
    r.style.setProperty('--main', rs.getPropertyValue('--black'));
    r.style.setProperty('--background', rs.getPropertyValue('--white'));
    r.style.setProperty('--invBackground', rs.getPropertyValue('--black'));
    r.style.setProperty('--accentBackground', rs.getPropertyValue('--almost-white'));
  }
}
//show information about Polygon(Calw)
selectElement(0)