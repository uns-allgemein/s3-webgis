# setup
To start this application there are only three stepts:
1. run 'npm install' to install all dependencies.
2. run 'npm start' to start the parcel-bundler to serve the dev-Application.
3. open in your Browser the url: http://localhost:1234

If this is not working I build an demo.
You find it at: https://bemastergis.mikewunderlich.de/webgis2/