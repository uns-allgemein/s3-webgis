// Elemente welche angezeigt werden sollen, sowie Informationen über die Stadt selbst.
var elemente = [
  {
    "title": "Calw",
    "type": "main",
    "content": '<h2>Calw</h2><img src="assets/wappen.png"><p>Calw (früher [kalp] ausgesprochen und dementsprechend manchmal Kalb geschrieben[2][3], heute wegen der Schreibung meist [kalf], aber [v] in „Calwer(in)“) ist eine Stadt in Baden-Württemberg, etwa 18 Kilometer südlich von Pforzheim und 33 Kilometer westlich von Stuttgart gelegen. Die Kreisstadt, zugleich die größte Stadt im Landkreis Calw, bildet ein Mittelzentrum für die umliegenden Gemeinden. Seit 1. Januar 1976 ist Calw Große Kreisstadt. Sie gehört zur Region Nordschwarzwald und zur Randzone der europäischen Metropolregion Stuttgart.</p><p class="quelle">Quelle: <a href="https://de.wikipedia.org/wiki/Calw" target="new">de.wikipedia.org/wiki/Calw</a></p>'
  },{
    "title": "der Lange",
    "type": "element",
    "position": {"x":535, "y":440},
    "content": '<h2>Der Lange</h2><img src="assets/derLange.jpg"><p>Der ehemalige Turm der Stadtbefestigung diente auch als Gefängnisturm. In dem unteren steinernen Teil befanden sich mehrere Gefängnisräume. Im oberen Fachwerkstock wurde eine kleine Wohnung für einen der beiden Wächter eingerichtet, die hier ehemals ihren Dienst taten. Neben der Betreuung der Gefangenen gehörte zu ihren Dienstpflichten das Ausrufen der Stunden und die allgemeine Wacht über die Stadt. Den Namen der »Lange« erhielt der Turm, weil er der höchste aller Türme war. Zudem war er an einer hochgelegenen Stelle errichtet worden. Optisch erschien er dadurch noch länger. Im »Langen« ist heute ein kleines Museum eingerichtet. Es gibt Auskunft über die frühere Befestigung der Stadt und gewährt Einblicke in die Nutzung des Turmes als Gefängnis. Über zahlreiche Stufen kann man in den Sommermonaten auf den Turm hinaufsteigen. Vorbei an den Gefängniszellen gelangt man zur ehemaligen Wächterwohnung. Von hier aus genießt man einen schönen Blick über Calw.</p><p>Im Zwinger - Als Zwinger bezeichnet man meist einen Graben, der bei einer Stadtbefestigung zwischen der eigentlichen Stadtmauer und einem zweiten, meist niedrigeren Hindernis (entweder ebenfalls eine Mauer oder eine dichte Hecke) liegt. Die Straße »Im Zwinger« zeichnet den Verlauf der ehemaligen Stadtbefestigung im Westen von Calw nach.</p><p class="quelle">Quelle: <a href="https://www.calw.de/DerLange" target="new">www.calw.de/DerLange</a></p>'
  },
  {
    "title": "Marktplatz",
    "type": "element",
    "position": {"x":590, "y":460},
    "content": '<h2>Marktplatz</h2><img src="assets/marktplatz.jpg"><p>Der Calwer Marktplatz zeigt ein weitgehend geschlossenes Fachwerkensemble. Die Häuser wurden Ende des 17. Jahrhunderts nach einem verheerenden Stadtbrand neu aufgebaut und waren einst bevorzugte Wohngegend der einflussreichen und vermögenden Calwer Handelsherren und Unternehmer.</p><p>Die beiden Marktbrunnen wurden erstmals 1523 erwähnt. Ihr heutiges Aussehen erhielten die Brunnen vermutlich 1686. Die Zuleitung des Wassers erfolgte bis ca. 1877 über sogenannte Teuchelleitungen. Die Brunnen dienten auch als Wasserspeicher zur Brandbekämpfung. Das Wasser reichte leider nicht aus, um die großen Stadtbrände in den Jahren 1634 und 1692 einzudämmen. Im Jahre 1622 hat der Steinmetz Hans Kessler den oberen Marktbrunnen für 760 Gulden neu erstellt. Der Brunnen trägt auf seiner Brunnensäule die Jahreszahl 1686 und einen Löwen, der das Württembergische und das Calwer Wappen hält. Der Löwe ist nicht nur das Wappentier der Calwer sondern auch Schildträger der Wappen des Herzogs von Württemberg.</p><p class="quelle">Quelle: <a href="https://www.calw.de/Marktplatz" target="new">www.calw.de/Marktplatz</a></p>'
  },
  {
    "title": "Nikolausbrücke",
    "type": "element",
    "position": {"x":635, "y":485},
    "content": '<h2>Nikolausbrücke</h2><img src="assets/nikolausbruecke.jpg"><p>Das Wahrzeichen der Stadt Calw ist die älteste Steinbrücke über die Nagold mit der Kapelle des Nikolaus auf dem Mittelpfeiler; sie wurde um 1400 erbaut, 1863/64 und 1926 erneuert; dabei wurde die Kapelle mit einem neuen Türmchen versehen. Die Brücke war für Hesse der ihm "liebste Platz im Städtchen“, selbst der „Domplatz von Florenz" schien ihm "nichts dagegen". Zum 125 jährigen Geburtstag Hermann Hesses im Jahr 2002 wurde eine lebensgroße Hermann-Hesse-Bronze-Skulptur des Künstlers Kurt Tassotti an diesem Platz aufgestellt.</p><p class="quelle">Quelle: <a href="https://www.calw.de/Ausflugsziel?view=publish&item=tripDestination&id=219" target="new">www.calw.de</a></p>'
  },
  {
    "title": "altes Kino",
    "type": "element",
    "position": {"x":590, "y":345},
    "content": '<h2>altes Kino</h2><img src="assets/altesKino.jpg"><p>Mit der festlichen Aufführung des Filmes „Unser eigenes Ich" wurde das "Volkstheater" Anfang 1951 wiedereröffnet.Der Saal mit 560 Plätzen war bühnenmäßig und mit Nebenräumen so ausgestattet, das neben seiner eigentlichen Bestimmung alle Möglichkeiten für Bühnenaufführungen gegeben waren. Innenarchitektonisch besonders attraktiv war der halbovale, mit einer Bar und beleuchteten Vitrinen ausgestattete Vorraum; von hier aus führten seitliche Aufgänge zum Parkett und dann weiter zum Rang. Für angenehme Bequemlichkeit sorgten die durchgehende Hochpolsterbestuhlung, erweiterte Sitzreihen und eine moderne Be- und Entlüftungsanlage. Bemerkenswert war auch die harmonische Bühnengestaltung mit farblich geschmackvoll abgestimmten Vorhängen.</p><p>Das große "Volkstheater" schloss mit dem Ausscheiden von Marlies Petry in de 80ern, während das kleinere "Cinema" seit 1997 von der Kinostar-Gruppe weiterbetrieben wurde, bevor es 2019 mit dem Film "Aladdin" ebenfalls geschlossen wurde. Das Gelände muss in den nächsten Jahren wegen eines Tunnelbaus abgerissen werden. Der Betreiber ist auf der Suche nach einem neuen Standort in Calw (Stand 2019)</p><p class="quelle">Quelle: <a href="http://www.allekinos.com/CALW%20NeuesCinema.htm" target="new">www.allekinos.com</a></p>'
  },
  {
    "title": "Schafott",
    "type": "element",
    "position": {"x":480, "y":620},
    "content": '<h2>Schafott</h2><img src="assets/schafott.jpg"><p>Das Calwer Schafott ist nur noch ein Blickfang für Spaziergänger - eine Richtstätte im Ruhestand. 1818 fand hier die letzte Hinrichtung aufgrund eines Gerichtsurteils unter großer Teilnahme der Bevölkerung statt. Neben dem Schafott gibt es eine Affenschaukel für Kinder.</p><p>Das Schafott ist zu Fuß wie folgt erreichbar: Vom Waldparkplatz „Zavelsteiner Sträßle" des Stadtteils Wimberg geradeaus in den Wald, nach ca. 100 m links der rot-schwarzen Wanderwegmarkierung Richtung Calw folgend. Nach ca. 500 m erreichen Sie auf der rechten Seite eine runde Steinbühne mit einer breiten Treppe - die Richtstätte.</p><p class="quelle">Quelle: <a href="https://www.calw.de/Ausflugsziel?view=publish&item=tripDestination&id=6" target="new">www.calw.de</a></p>'
  }
]
// flag
var darkMode = false;

// Ein Element der Navigation hinzufügen
function addElement2ul(item){
  var ul = document.querySelectorAll('ul')[0];//document.getElementById("list");
  var li = document.createElement("li");
  li.appendChild(document.createTextNode(item.title));
  li.title = item.title;
  if (item.type === "main"){
    li.classList.add('focus');
  }
  ul.appendChild(li);
}

// Ein Element der Karte hinzufügen
function addIcon2Map(title, top, left){
  var map = document.getElementById('map');
  var img = document.createElement("img");
  img.src="assets/info.png";
  img.classList.add("icon");
  img.style="top:"+top+"px; left:"+left+"px;"
  img.title = title;
  map.appendChild(img);
}
// Durch alle Elemente iterieren und diese der Navigation und Karte hinzufügen
elemente.forEach(item => {
  switch (item.type) {
    case "main":
      document.getElementById('main').innerHTML = item.content;
      break;
    case "element":
      addIcon2Map(item.title, item.position.y, item.position.x);
      break;
  }
  addElement2ul(item);
})

// Interaktion-Listener für alle Elemente (Navigation und Karte)
document.querySelectorAll('li, .icon').forEach(item => {
  item.addEventListener('click', click2Element)
});

// Function für die Interaktion
function click2Element(event){
  // Index des Elements erhalten und das HTML des Main-Bereichs anpassen.
  var index = elemente.map(function(e) { return e.title; }).indexOf(event.target.title);
  document.getElementById('main').innerHTML = elemente[index].content

  // die "focus" Klasse verwalten.
  var elements = document.getElementsByClassName('focus');
  while(elements.length > 0){
    elements[0].classList.remove('focus');
  }
  // Navigation
  document.querySelectorAll('li[title="'+elemente[index].title+'"]')[0].classList.add("focus");
  // Karte
  document.querySelectorAll('img[title="'+elemente[index].title+'"]')[0].classList.add("focus");
}


// DarkMode togglen
document.getElementById('darkmode').addEventListener('click',switchDarkmode);
function switchDarkmode(){
  console.log("test");
  var r = document.querySelector(':root');
  var rs = getComputedStyle(r);
  darkMode = !darkMode;
  if(darkMode){
    document.getElementById('map').style.backgroundImage = "url(./assets/map_dark.jpg)";
    r.style.setProperty('--main', rs.getPropertyValue('--white'));
    r.style.setProperty('--background', rs.getPropertyValue('--almost-black'));
    r.style.setProperty('--invBackground', rs.getPropertyValue('--white'));
    r.style.setProperty('--accentBackground', rs.getPropertyValue('--black'));
  } else{
    document.getElementById('map').style.backgroundImage = "url(./assets/map_topo.jpg)";
    r.style.setProperty('--main', rs.getPropertyValue('--black'));
    r.style.setProperty('--background', rs.getPropertyValue('--white'));
    r.style.setProperty('--invBackground', rs.getPropertyValue('--black'));
    r.style.setProperty('--accentBackground', rs.getPropertyValue('--almost-white'));
  }
}