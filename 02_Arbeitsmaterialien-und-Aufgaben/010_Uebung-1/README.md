Getestet unter folgenden Browser:
- Firefox 86.0
- Google Chrome 89.0.4389.72
- Microsoft Edge 89.0.774.45
- Microsoft Internet Explorer 1909 - Funktioniert nicht.

Der Internet-Explorer kennt einige neuere Javascript befehle nicht. In einem live-Projekt würde dies selbstverständlich überarbeitet werden.