# setup
To start this application there are only three stepts:
1. run 'npm install' to install all dependencies.
2. run 'npm start' to start the parcel-bundler to serve the dev-Application.
3. open in your Browser the url: http://localhost:1234

If this is not working I build an demo.
You find it at: https://bemastergis.mikewunderlich.de/webgis3/

# Geoserver Setup
My Geoserver is hostet on my root-server.
To deploy it there I decided to use Docker, so I created an Dockerfile by my own to learn more about it.

to run the Docker-Image you have to install docker and just enter this command:
docker run -d -p 8081:8080 \\
    -e GEOSERVER_CSRF_WHITELIST=geoserver1.undefined-solutions.de \\
    -v /usr/docker/geoserver8081:/usr/share/geoserver/data_dir \\
    --name geoserver8081 undefinedsolutions/geoserver:2.18.2

To Avoid CORS-Problems I decided to follow the Geoserver guideline:
https://docs.geoserver.org/latest/en/user/production/container.html#enable-cors

After this I created an workspace for this project, it is named cwLindenrain.
    Lindenrain is the name of the industrial area.
And uploaded all three GeoTiff-Files.
In preparation i projected them to 3857 and create all available pyramids.