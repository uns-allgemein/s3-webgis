import 'ol/ol.css';
import {Map, View} from 'ol';
import TileLayer from 'ol/layer/Tile';
import TileWMS from 'ol/source/TileWMS';
import OSM from 'ol/source/OSM';
import {transformExtent} from 'ol/proj';
import {fromLonLat} from 'ol/proj';
import WMSCapabilities from 'ol/format/WMSCapabilities';
import {getRenderPixel} from 'ol/render';

var parser = new WMSCapabilities();

// transform extent 4326 to OL-3857
function transform(extent) {
  //[minX, minY, maxX, maxY]
  return transformExtent(extent, 'EPSG:4326', 'EPSG:3857');
}
// Add element to select
function addElement(htmlElement, arrayLayer){
  arrayLayer.reverse().forEach(function (layer) {
    var option = document.createElement("option");
    option.text = layer;
    htmlElement.add(option);
  })
}
function addLayerToUI(arrayLayer){
  addElement( document.getElementById("layers1"),arrayLayer);
  addElement( document.getElementById("layers2"),arrayLayer);
}
document.getElementById('layers1').addEventListener('change', switchLayer)
document.getElementById('layers2').addEventListener('change', switchLayer)

function switchLayer(event){
  var layersElement = document.getElementById(event.target.id);
  var layer = layersElement.options[layersElement.selectedIndex].text;
  // select the correct layer and change the Source
  map.getLayers().item(event.target.id[event.target.id.length-1]).setSource(
    new TileWMS({
      url: urlWMS,
      params: {'LAYERS': layer, 'TILED': true},
      serverType: 'geoserver',
      transition: 1
    })
  );
  // show or hide the swipe-bar
  if (map.getLayers().item(1).getSource().getParams().LAYERS
      ===
      map.getLayers().item(2).getSource().getParams().LAYERS)
  {
    document.getElementById('swipe').style.visibility = "hidden";
  } else{
    document.getElementById('swipe').style.visibility = "visible";
  }
}

const map = new Map({
  target: 'map',
  layers: [
    new TileLayer({
      source: new OSM()
    })
  ],
  view: new View({
    center: fromLonLat([8.773112503431266,48.67901959296768]),
    zoom: 16
  })
});

var urlWMS;
var layerLeft;
var layerRight;
// parse the GetCapabilities to get the URL and the Layers
fetch('https://geoserver1.undefined-solutions.de/cwLindenrain/wms?SERVICE=WMS&VERSION=1.3.0&REQUEST=GetCapabilities').then(function(response) {
  return response.text();
}).then(function(text) {
  var result = parser.read(text);
  var layers = result.Capability.Layer.Layer.map(function(item){
    return item.Title
  });
  var extentWMS = result.Capability.Layer.EX_GeographicBoundingBox;
  urlWMS = result.Capability.Request.GetMap.DCPType[0].HTTP.Get.OnlineResource;

  // create two new Layer, left and right
  layerLeft = new TileLayer({
    preload: Infinity,
    extent: transform(extentWMS),
    source: new TileWMS({
      url: urlWMS,
      params: {'LAYERS': layers[layers.length-1], 'TILED': true},
      serverType: 'geoserver',
      transition: 1
    })
  })

  map.getLayers().push(layerLeft);
  layerRight = new TileLayer({
    preload: Infinity,
    extent: transform(extentWMS),
    source: new TileWMS({
      url: urlWMS,
      params: {'LAYERS': layers[0], 'TILED': true},
      serverType: 'geoserver',
      transition: 1
    })
  })
  map.getLayers().push(layerRight);

  addLayerToUI(layers)
  
  // Add prerenderer to right layer
  // Source: https://openlayers.org/en/latest/examples/layer-swipe.html
  layerRight.on('prerender', function (event) {
    var ctx = event.context;
    var mapSize = map.getSize();
    var width = mapSize[0] * (swipe.value / 100);
    var tl = getRenderPixel(event, [width, 0]);
    var tr = getRenderPixel(event, [mapSize[0], 0]);
    var bl = getRenderPixel(event, [width, mapSize[1]]);
    var br = getRenderPixel(event, mapSize);

    ctx.save();
    ctx.beginPath();
    ctx.moveTo(tl[0], tl[1]);
    ctx.lineTo(bl[0], bl[1]);
    ctx.lineTo(br[0], br[1]);
    ctx.lineTo(tr[0], tr[1]);
    ctx.closePath();
    ctx.clip();
  });

  layerRight.on('postrender', function (event) {
    var ctx = event.context;
    ctx.restore();
  });
});

// Swipe
var swipe = document.getElementById('swipe');
swipe.addEventListener(
  'input',
  function () {
    map.render();
  },
  false
);