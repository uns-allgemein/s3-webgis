import headingSelected from "/assets/heading-severity5.png";

import 'ol/ol.css';
import {Map, View} from 'ol';
import {OSM, Vector as VectorSource} from 'ol/source';
import {Circle as CircleStyle, Fill, Icon, Stroke, Style, Text} from 'ol/style';
import Feature from 'ol/Feature';
import Point from 'ol/geom/Point';
import {Tile as TileLayer, Vector as VectorLayer} from 'ol/layer';
import {fromLonLat} from 'ol/proj';
import exifr from 'exifr';

import Projection from 'ol/proj/Projection';
import ImageLayer from 'ol/layer/Image';
import Static from 'ol/source/ImageStatic';
import {getCenter} from 'ol/extent';
import Draw, {createBox} from 'ol/interaction/Draw';
import {defaults} from 'ol/control';


document.getElementById("upload").addEventListener("change",onPick);
var galerieElement = document.getElementById('galerie');
// Helper functions
function deg2rad(deg){
  return deg * Math.PI / 180;
}
function ConvertDMSToDD(degrees, minutes, seconds, direction) {
  var dd = degrees + minutes/60 + seconds/(60*60);
  if (direction == "S" || direction == "W") {
      dd = dd * -1;
  }
  return dd;
}

function onPick(e) {
  var files = e.target.files;

  if(files) {
    for (let i = 0, f; f = files[i]; i++) {
      var reader = new FileReader();
      reader.onloadend = function(output){
        exifr.parse(f, {tiff: true, xmp: true}).then(exif => addPoint(exif, f))

        //var img = document.createElement("img");
        //img.addEventListener("click", changeImage);
        //img.src=output.target.result;
        //document.getElementById('galerie').appendChild(img);
      }.bind(this);
      reader.readAsDataURL(f)
    }
  }
}
function changeImage(event){
  viewImage(event.target.currentSrc, [0, 0, event.target.naturalWidth, event.target.naturalHeight]);
}

function addPoint(exif, file){
  // Label;X/East;Y/North;Z/Altitude;           Yaw;Pitch;Roll; Error_(m);  X_error;Y_error;Z_error;Error_(deg);Yaw_error;Pitch_error;Roll_error;X_est;Y_est;Z_est;Yaw_est;Pitch_est;Roll_est
  // DJI_0304.JPG;8.774170;48.534700;609.736000;;;;;;;;;;;;;;;;;

  var exifString = ''
  exifString += file.name
  exifString += ';' + exif.GpsLongtitude
  exifString += ';' + exif.GpsLatitude
  exifString += ';' + exif.AbsoluteAltitude
  exifString += ';' + exif.GimbalYawDegree
  exifString += ';' + exif.GimbalPitchDegree
  exifString += ';' + exif.GimbalRollDegree
  var errm = exif.RtkStdLon
  if (exif.RtkStdLat > exif.RtkStdLon){
    errm = exif.RtkStdLat
  }
  exifString += ';' + errm
  exifString += ';' + exif.RtkStdLon
  exifString += ';' + exif.RtkStdLat
  exifString += ';' + exif.RtkStdHgt+';;;;;;;;;<br>'
  galerieElement.innerHTML = galerieElement.innerHTML + exifString 
  //
}
