import headingSelected from "/assets/heading-severity5.png";

import 'ol/ol.css';
import {Map, View} from 'ol';
import {OSM, Vector as VectorSource} from 'ol/source';
import {Circle as CircleStyle, Fill, Icon, Stroke, Style, Text} from 'ol/style';
import Feature from 'ol/Feature';
import Point from 'ol/geom/Point';
import {Tile as TileLayer, Vector as VectorLayer} from 'ol/layer';
import {fromLonLat} from 'ol/proj';
import exifr from 'exifr';

import Projection from 'ol/proj/Projection';
import ImageLayer from 'ol/layer/Image';
import Static from 'ol/source/ImageStatic';
import {getCenter} from 'ol/extent';
import Draw, {createBox} from 'ol/interaction/Draw';
import {defaults} from 'ol/control';


document.getElementById('clear').addEventListener('click',clearImages);
function clearImages(){
  vectorSource.clear();
  var galerieElement = document.getElementById('galerie');
  while(galerieElement.hasChildNodes()){
    galerieElement.removeChild(galerieElement.childNodes[0]);
  }
  
}
document.getElementById("upload").addEventListener("change",onPick);
// Helper functions
function deg2rad(deg){
  return deg * Math.PI / 180;
}
function ConvertDMSToDD(degrees, minutes, seconds, direction) {
  var dd = degrees + minutes/60 + seconds/(60*60);
  if (direction == "S" || direction == "W") {
      dd = dd * -1;
  }
  return dd;
}

function onPick(e) {
  var files = e.target.files;

  if(files) {
    for (let i = 0, f; f = files[i]; i++) {
      var reader = new FileReader();
      reader.onloadend = function(output){
        exifr.parse(f, {tiff: true, xmp: true}).then(exif => addPoint(exif, output.target.result))

        var img = document.createElement("img");
        img.addEventListener("click", changeImage);
        img.src=output.target.result;
        document.getElementById('galerie').appendChild(img);
      }.bind(this);
      reader.readAsDataURL(f)
    }
  }
}
function changeImage(event){
  viewImage(event.target.currentSrc, [0, 0, event.target.naturalWidth, event.target.naturalHeight]);
}

function addPoint(exif, src){
  console.log(exif);

  if (exif.Orientation === "Rotate 90 CW"){
    viewImage(src, [0, 0, exif.ExifImageHeight, exif.ExifImageWidth]);
  } else {
    viewImage(src, [0, 0, exif.ExifImageWidth, exif.ExifImageHeight]);
  }
  
  var exifDic = {
    "GPSLongitude": ConvertDMSToDD(exif.GPSLongitude[0],exif.GPSLongitude[1],exif.GPSLongitude[2],exif.GPSLongitudeRef),
    "GPSLatitude": ConvertDMSToDD(exif.GPSLatitude[0],exif.GPSLatitude[1],exif.GPSLatitude[2],exif.GPSLatitudeRef),
    "GimbalYawDegree": exif.GimbalYawDegree,
    "RelativeAltitude": exif.RelativeAltitude
  }
  if (!(isNaN(exifDic["RelativeAltitude"]))){
    exifDic["RelativeAltitude"] = exifDic["RelativeAltitude"].toString()
  }
  vectorSource.addFeature(
    new Feature({
    geometry: new Point(fromLonLat([exifDic["GPSLongitude"],exifDic["GPSLatitude"] ])),
    altitude: exifDic["RelativeAltitude"],
    rotation: deg2rad(exifDic["GimbalYawDegree"])
  }))
  map.getView().fit(vectorSource.getExtent(),{padding:[100,100,100,100],maxZoom:20});
}

var positionStyle = new Style({
  image: new CircleStyle({
    radius: 7,
    fill: new Fill({color: 'red'}),
    stroke: new Stroke({
      color: 'white',
      width: 1,
    }),
  }),

});
var headingStyle = new Style({
  image: new Icon({
    anchor: [0.5, 26],
    anchorXUnits: 'fraction',
    anchorYUnits: 'pixels',
    src: headingSelected,
    scale: 1,
  })
});

var labelStyle = new Style({
  text: new Text({
    font: '25px Calibri,sans-serif',
    overflow: true,
    fill: new Fill({
      color: '#000'
    }),
    stroke: new Stroke({
      color: '#fff',
      width: 1.5
    }),
    offsetY: 0
  })
});

var vectorSource = new VectorSource({
  features: []
})

var style = [positionStyle, headingStyle, labelStyle];

var map = new Map({
  target: 'map',
  layers: [
    new TileLayer({
      source: new OSM()
    }),
    new VectorLayer({
      source: vectorSource,
      style: function(feature) {
          //labelStyle.getText().setText(feature.get('altitude'));
          headingStyle.getImage().setRotation(feature.get('rotation'));
          return style;
      }
    })
  ],
  view: new View({
    center: fromLonLat([0,0]),
    zoom: 5
  })
});

var source = new VectorSource({wrapX: false});

var draw;
var vector = new VectorLayer({
  source: source,
});

var extent = [0, 0, 1024, 1024];
var projection = new Projection({
  code: 'xkcd-image',
  units: 'pixels',
  extent: [0, 0, 99999999, 99999999],
});

var imageMap = new Map({
  layers: [
    new ImageLayer({
      source: new Static({
        url: '',
        projection: projection,
        imageExtent: extent,
      }),
    }) ,
    vector
  ],
  target: 'image',
  view: new View({
    projection: projection,
    center: getCenter(extent),
    zoom: 2
  }),
  controls : defaults({
    attribution : false,
    zoom : false,
  }),
});
imageMap.getView().fit(extent,{padding:[25,25,25,25]});

function viewImage(inGetURL, inExtent){
  extent = inExtent;
  imageMap.getLayers().removeAt(0);
  imageMap.getLayers().insertAt(0,
    new ImageLayer({
      source: new Static({
        url: inGetURL,
        projection: projection,
        imageExtent: extent,
      }),
    })
  );
  imageMap.getView().fit(extent,{padding:[25,25,25,25]});
}


//click functions
// maps
document.getElementById('mapCenter').addEventListener('click',mapCenter)
function mapCenter(){
  console.log("mapCenter");
  if (vectorSource.getFeatures().length > 0){
    map.getView().fit(vectorSource.getExtent(),{padding:[100,100,100,100],maxZoom:20});
  }  
}
// image
document.getElementById('imageCenter').addEventListener('click',imageCenter)
function imageCenter(){
  console.log("imageCenter");
  imageMap.getView().fit(extent,{padding:[25,25,25,25]});
}

//draw rectangle
document.getElementById('imageCreateRectangle').addEventListener('click',imageCreateRectangle)
function imageCreateRectangle(){
  console.log('test');

  var geometryFunction = createBox()
  draw = new Draw({
    source: source,
    type: 'Circle',
    geometryFunction: geometryFunction,
  });
  console.log(source.getFeatures());
  
  imageMap.addInteraction(draw);
}
/*
imageMap.on('click', imageMapClick);
function imageMapClick(evt){
  console.log(evt.coordinate);
}*/

/*
Action
function addInteraction() {
  
  var value = 'Circle';
  var geometryFunction = createBox()
  if (value !== 'None') {
    draw = new Draw({
      source: source,
      type: value,
      geometryFunction: geometryFunction,
    });
    
    imageMap.addInteraction(draw);
  }
}
//addInteraction();
*/