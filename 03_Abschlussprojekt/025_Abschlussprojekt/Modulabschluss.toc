\babel@toc {nil}{}
\contentsline {section}{\numberline {1}Verwendete Frameworks}{2}{section.1}%
\contentsline {subsection}{\numberline {1.1}Angular}{2}{subsection.1.1}%
\contentsline {subsection}{\numberline {1.2}Material-Design}{2}{subsection.1.2}%
\contentsline {subsection}{\numberline {1.3}Openlayers}{2}{subsection.1.3}%
\contentsline {section}{\numberline {2}Setup}{2}{section.2}%
\contentsline {section}{\numberline {3}Beschreibung des Projekts}{3}{section.3}%
\contentsline {subsection}{\numberline {3.1}Komponenten}{3}{subsection.3.1}%
\contentsline {subsubsection}{\numberline {3.1.1}Table of Contents}{3}{subsubsection.3.1.1}%
\contentsline {subsubsection}{\numberline {3.1.2}image-view}{3}{subsubsection.3.1.2}%
\contentsline {subsubsection}{\numberline {3.1.3}map-view}{3}{subsubsection.3.1.3}%
\contentsline {subsubsection}{\numberline {3.1.4}side-nav-detail}{3}{subsubsection.3.1.4}%
\contentsline {subsubsection}{\numberline {3.1.5}side-nav-features-detail}{4}{subsubsection.3.1.5}%
\contentsline {subsubsection}{\numberline {3.1.6}side-nav-image-detail}{4}{subsubsection.3.1.6}%
\contentsline {subsection}{\numberline {3.2}Services}{4}{subsection.3.2}%
\contentsline {subsubsection}{\numberline {3.2.1}mockup-data}{4}{subsubsection.3.2.1}%
\contentsline {subsubsection}{\numberline {3.2.2}side-nav}{4}{subsubsection.3.2.2}%
\contentsline {section}{\numberline {4}Ausblick}{4}{section.4}%
